export const environment = {
  production: true,
  fileInfoService: '/cabinet/file',
  dirInfoService: '/cabinet/dir'
};
