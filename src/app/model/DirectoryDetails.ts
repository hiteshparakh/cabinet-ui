import { File } from "./File";

export interface DirectoryDetails {
    inputDirectoryPath: string;
	files: File[];
	countOfFiles: number;
}