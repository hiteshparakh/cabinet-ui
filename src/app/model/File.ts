export interface File {
    name: string;
	parentName: string;
	absPath: string;
	directory: boolean;
	canExecute: boolean;
	canRead: boolean;
	canWrite: boolean;
	hidden: boolean;
	size: number;
}