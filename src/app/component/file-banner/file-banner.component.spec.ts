import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileBannerComponent } from './file-banner.component';

describe('FileBannerComponent', () => {
  let component: FileBannerComponent;
  let fixture: ComponentFixture<FileBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
