import { Component, OnInit } from '@angular/core';
import { FileDetailService } from 'src/app/service/filedetail-service';
import { File } from 'src/app/model/File';

@Component({
  selector: 'app-file-detail',
  templateUrl: './file-detail.component.html',
  styleUrls: ['./file-detail.component.scss'],
  providers: [FileDetailService]
})
export class FileDetailComponent implements OnInit {

  files: File[];
  loading: boolean;

  constructor(private fileDetailsService: FileDetailService) { }

  ngOnInit(): void {
  }

  fetchDirDetails(path: string): void {
    this.clearGrid();
    this.loading = true;
    this.fileDetailsService.getDirectoryListing(path).subscribe(dirDetailsObj => {
      this.files = dirDetailsObj.files;
      this.loading = false;
    });
  }

  fetchFileDetails(path: string): void {
    this.clearGrid();
    this.fileDetailsService.getFileDetails(path).subscribe(file => {
      this.files = Array.of(file);
    });
  }

  clearGrid(): void {
    this.files = [];
  }
}
