import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { DirectoryDetails } from 'src/app/model/DirectoryDetails';
import { File } from 'src/app/model/File';
import { Observable, from } from 'rxjs';
import { catchError } from 'rxjs/operators';
// import 'rxjs/add/observable/throw';
import { throwError } from 'rxjs';

@Injectable()
export class FileDetailService {

    private fileInfoServiceUrl = environment.fileInfoService;
    private dirInfoServiceUrl = environment.dirInfoService;

    constructor(private httpClient : HttpClient) {

    }

    getDirectoryListing(dirPath: string): Observable<DirectoryDetails> {
        const url = `${this.dirInfoServiceUrl}/listDetails`;
        const params = new HttpParams ({
            fromObject: {
                'dirPath': dirPath
            }
        });

        return this.httpClient.get<DirectoryDetails>(url, {params: params}).pipe(catchError(this.handleError));
    }

    getFileDetails(filePath: string): Observable<File> {
        const url = `${this.fileInfoServiceUrl}/info`;
        const params = new HttpParams ({
            fromObject: {
                'filePath': filePath
            }
        });

        return this.httpClient.get<File>(url, {params: params}).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        return throwError(error);
    }
}