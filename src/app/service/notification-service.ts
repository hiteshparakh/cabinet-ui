import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
// import { publish } from "rxjs/operators";

@Injectable()
export class NotificationService {
    
    public _notification: Subject<string> = new Subject<string>();
    
    constructor () {}

    notify(message) {
        this._notification.next(message);
    }
}