import { Component, ChangeDetectorRef } from '@angular/core';
import { NotificationService } from './service/notification-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cabinet-ui';
  showErrorPopup = false;
  errorMsg: string = '';

  constructor(private notificationService: NotificationService, private changeDetectorRef: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.notificationService._notification.subscribe(message => {
      this.errorMsg = message;
      this.showErrorPopup = true;
      this.changeDetectorRef.detectChanges();
    })
  }
}
