import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FileDetailComponent } from './component/file-detail/file-detail.component';
import { FileDetailService } from './service/filedetail-service'

import { ErrorsModule } from './errorhandler/errors.module';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { NotificationService } from './service/notification-service';
import { FileBannerComponent } from './component/file-banner/file-banner.component';

@NgModule({
  declarations: [
    AppComponent,
    FileDetailComponent,
    FileBannerComponent
  ],
  imports: [
    BrowserModule,    
    BrowserAnimationsModule,
    TableModule,
    HttpClientModule,
    ErrorsModule,
    DialogModule
  ],
  providers: [ FileDetailService, NotificationService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
