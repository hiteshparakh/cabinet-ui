import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from '../service/notification-service';

@Injectable()
export class GlobalErrorsHandler implements ErrorHandler  {

    constructor(private notificaitonService: NotificationService) {

    }

    handleError(error : HttpErrorResponse) {
        this.notificaitonService.notify(`${error.error}`);
    }
}