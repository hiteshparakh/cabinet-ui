import { NgModule, ErrorHandler } from "@angular/core";
import { CommonModule } from "@angular/common"
import { GlobalErrorsHandler } from "./global-error-handler";

@NgModule({
    declarations: [
      
    ],
    imports: [
      CommonModule
    ],
    providers: [ 
        {
            provide: ErrorHandler,
            useClass: GlobalErrorsHandler
        }
    ]
  })
export class ErrorsModule {}